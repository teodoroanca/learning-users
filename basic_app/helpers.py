def give_me_some_more(username, number_of_children):
    """Describe what the function does.

    :param username: Why do we need this param
    :param number_of_children: Why do we need this param
    :type username: str
    :type number_of_children: int
    :return: Some strange operation with number of children
    :rtype: int
    """
    if username == 'Mark':
        return number_of_children * 2
    else:
        return number_of_children * 309

from django.db import models
from django.contrib.auth.models import User  # Cand folosesc useri adaug asta
# Create your models here.


class UserProfileInfo(models.Model):
    """User profile info."""

    user = models.OneToOneField(User, on_delete=models.PROTECT)

    # Additional
    portofolio_site = models.URLField(blank=True)

    profile_pic = models.ImageField(upload_to='profile_pics', blank=True)

    def __str__(self):
        return self.user.username

from django.contrib import admin
from basic_app.models import UserProfileInfo
# Register your models here.


@admin.register(UserProfileInfo)
class UserProfileInfoAdmin(admin.ModelAdmin):
    """UserProfileInfo admin."""

    list_display = ['user', 'portofolio_site']

    def get_list_display(self, request):
        """Thread safe solution.

        :param request:
        :return:
        """
        list_display = self.list_display[:]
        if request.user.username == 'root':
            if 'profile_pic' not in self.list_display:
                list_display.append('profile_pic')

        return list_display

    # def get_queryset(self, request):
    #     """Not a thread safe way.
    #
    #     :param request:
    #     :return:
    #     """
    #     if request.user.username == 'root':
    #         if 'profile_pic' not in self.list_display:
    #             self.list_display.append('profile_pic')
    #
    #     return super(UserProfileInfoAdmin, self).get_queryset(request)

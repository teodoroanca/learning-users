==============
Learning users
==============
What this app does.

Installation
============

.. code-block:: sh

    pip install -r requirements.txt

Usage
=====

Some code sample:

.. code-block:: python

    from basic_app.models import User
    User.objects.all()

Testing
=======

.. code-block:: sh

    python manage.py test basic_app
